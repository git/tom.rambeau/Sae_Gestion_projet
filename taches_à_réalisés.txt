
Ce qu'on doit faire:
- Description succinte du sujet de la Saé prise
-Genèse de l'idée de projet( d'où vient l'idée, pour quel client/entreprise ?)

-Maitre d'oeuvre, maître d'ouvrage et équipe de projet
	->Comment organisez-vous votre équipe ? Y-a-t-il chef de projet ? Si oui pourquoi ? Si non pourquoi ?
	->Quels sont les rôles de chaqu'un ? Et pourquoi avez-vous choisi une telle répartition ?
	->Y-t-il des réunions ? Si oui pourquoi ? Si non pourquoi ?
	->Quels outils avez-vous choisi pour communiquer et pourquoi ?
//Vous pouvez aussi nous présenter également d'autres éléments pour que nous comprenions comment vous travailler en équipe.

-Transcription des besoins (diagramme corne, diagramme pieuvre: quelle fonction doit remplir votre projet et pourquoi ?
 
-Description des livrables (Que devez-vous obtenir à la fin de votre projet ?)

-Niveau de confidentialité (Si un client réel vous avait demandé un tel projet, quel aurait été son niveau de confidentialité ?)




